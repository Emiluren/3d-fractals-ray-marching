![screenshot](screenshot.jpg)

# Description
This is a GLSL raymarching renderer for 3d fractals, specifically the
Mandelbulb fractal. To render something else you should change
`distance_function` in [src/raymarching.frag](src/raymarching.frag).
The application was written in rust and uses Glium.

# Controls
* Left/Right arrow keys - Spin camera left/right
* Up/Down - Move camera up/down
* W/S - Move camera closer/further away
