#[macro_use] extern crate glium;

use glium::glutin;
use glium::Surface;
use glium::index::PrimitiveType;
use glium::draw_parameters::{BackfaceCullingMode, PolygonMode};
use glium::glutin::{
    Api, ControlFlow, ElementState, Event, GlProfile, GlRequest, KeyboardInput,
    VirtualKeyCode, WindowEvent
};
use glium::program::ProgramChooserCreationError::ProgramCreationError;
use glium::debug::DebugCallbackBehavior;
use nalgebra::Point3;

use std::f32;
use std::fs;

#[derive(Copy, Clone)]
struct Vertex {
    vert_position: [f32; 2],
}

implement_vertex!(Vertex, vert_position);

// Scancodes for layout independence
const SCANCODE_W: u32 = 25;
const SCANCODE_S: u32 = 39;

fn main() {
    let mut events_loop = glutin::EventsLoop::new();
    let window_builder = glutin::WindowBuilder::new()
        .with_title("Raymarching")
        .with_dimensions(1024, 768);
    let context = glutin::ContextBuilder::new()
        .with_vsync(true)
        .with_gl_profile(GlProfile::Core)
        .with_gl(GlRequest::Specific(Api::OpenGl, (4, 3)))
        .with_depth_buffer(24);

    let window =
        glutin::GlWindow::new(
            window_builder, context, &events_loop
        ).expect("failed to create window");

    let display = glium::Display::with_debug(
        window, DebugCallbackBehavior::PrintAll
    ).expect("failed to create display");

    let handle_shader_error = |program_res| match program_res {
        Ok(prog) => prog,
        Err(ProgramCreationError(err)) => {
            panic!("{}", err)
        },
        e => {
            e.unwrap()
        }
    };

    let program = handle_shader_error(program!(
        &display,
        330 => {
            vertex: &fs::read_to_string("src/raymarch.vert").unwrap(),
            fragment: &fs::read_to_string("src/raymarch.frag").unwrap(),
        },
    ));

    let mut camera_yaw = 0.0_f32;
    let mut camera_radius = 2.0;
    let mut camera_pitch = 0.0_f32;

    let plane_vertices = [
        Vertex { vert_position: [-1.0, -1.0] },
        Vertex { vert_position: [1.0, -1.0] },
        Vertex { vert_position: [-1.0, 1.0] },
        Vertex { vert_position: [1.0, 1.0] },
    ];
    let vertex_buffer: glium::VertexBuffer<Vertex> =
        glium::VertexBuffer::new(&display, &plane_vertices)
            .expect("failed to create vertex buffer");

    let plane_indices = [
        0, 2, 1,
        1, 2, 3,
    ];
    let index_buffer: glium::IndexBuffer<u32> =
        glium::IndexBuffer::new(&display, PrimitiveType::TrianglesList, &plane_indices)
            .expect("failed to create index buffer");

    let render = |camera_yaw: f32, camera_radius: f32, camera_pitch: f32| {
        let mut surface = display.draw();
        surface.clear_color_and_depth((0.024, 0.184, 0.337, 0.0), 1.0);

        let camera_pos = Point3::new(
            camera_yaw.cos() * camera_radius * camera_pitch.cos(),
            camera_pitch.sin() * camera_radius,
            camera_yaw.sin() * camera_radius * camera_pitch.cos()
        );

        let (view_w, view_h) = display.get_framebuffer_dimensions();
        let aspect = view_w as f32 / view_h as f32;

        let uniforms = uniform! {
            aspect_ratio: aspect,
            eye_pos: Into::<[f32; 3]>::into(camera_pos.coords),
        };

        let draw_parameters = glium::DrawParameters {
            depth: glium::Depth {
                test: glium::DepthTest::IfLess,
                write: true,
                ..Default::default()
            },
            point_size: Some(8.0),
            polygon_mode: PolygonMode::Fill,
            backface_culling: BackfaceCullingMode::CullCounterClockwise,
            ..Default::default()
        };

        surface
            .draw(
                &vertex_buffer,
                &index_buffer,
                &program,
                &uniforms,
                &draw_parameters,
            )
            .expect("failed to draw to surface");

        surface.finish().expect("failed to finish rendering frame");
    };

    render(camera_yaw, camera_radius, camera_pitch);

    events_loop.run_forever(|event| {
        match event {
            Event::WindowEvent { event, .. } => {
                match event {
                    WindowEvent::Closed => return ControlFlow::Break,
                    WindowEvent::Resized(_, _) => render(camera_yaw, camera_radius, camera_pitch),
                    WindowEvent::KeyboardInput {
                        input: KeyboardInput {
                            state: ElementState::Pressed,
                            virtual_keycode,
                            scancode,
                            ..
                        },
                        ..
                    } => {
                        match scancode {
                            SCANCODE_W => {
                                camera_radius *= 0.95;
                            }
                            SCANCODE_S => {
                                camera_radius *= 1.05;
                            }
                            _ => (),
                        }
                        match virtual_keycode {
                            Some(VirtualKeyCode::Escape) => return ControlFlow::Break,
                            Some(VirtualKeyCode::Left) => {
                                camera_yaw += 0.05;
                            }
                            Some(VirtualKeyCode::Right) => {
                                camera_yaw -= 0.05;
                            }
                            Some(VirtualKeyCode::Up) => {
                                camera_pitch = (camera_pitch + 0.05).min(3.1 / 2.0);
                            }
                            Some(VirtualKeyCode::Down) => {
                                camera_pitch = (camera_pitch - 0.05).max(-3.1 / 2.0);
                            }
                            _ => (),
                        }
                        render(camera_yaw, camera_radius, camera_pitch);
                    },
                    _ => (),
                }
            },
            _ => (),
        }

        ControlFlow::Continue
    });
}
